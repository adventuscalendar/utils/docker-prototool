# We need Debian because of Protoc (Alpine messes up)
FROM debian:stretch-slim

ARG PROTOTOOL_VERSION

RUN apt-get update                                                                                              &&  \
    apt-get install -y                                                                                              \
        curl                                                                                                        \
        git                                                                                                     &&  \
    curl -SL https://github.com/uber/prototool/releases/download/${PROTOTOOL_VERSION}/prototool-Linux-x86_64        \
         -o /usr/local/bin/prototool                                                                            &&  \
    chmod +x /usr/local/bin/prototool

# No ENTRYPOINT or CMD because this is thought to be used as a build env for CI