# This build only makes sense in the CI context
ARG BASE_IMAGE
FROM ${BASE_IMAGE}

RUN curl -SL https://dl.google.com/go/go1.11.linux-amd64.tar.gz -o /tmp/go.tar.gz   &&  \
    tar -C /usr/local -xzf /tmp/go.tar.gz                                           &&  \
    rm /tmp/go.tar.gz

ENV PATH $PATH:/usr/local/go/bin:/root/go/bin

RUN go get                                              \
       github.com/gogo/protobuf/proto                   \
       github.com/gogo/protobuf/protoc-gen-gogoslick    \
       github.com/gogo/protobuf/gogoproto